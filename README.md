#A bookmarking system for [cmder](http://cmder.net/)#

Usage should resemble https://github.com/flavio/jump

To install add **jump.bat** to **cmder\bin** and **jump.lua** to **cmder\vendor\clink-completions**.