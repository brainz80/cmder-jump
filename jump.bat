@echo off

set JUMPS=%CMDER_ROOT%\config\jump
setlocal
:: handle quotes within command definition, e.g. quoted long file names
set _x="%*"
set _x=%_x:"=%

if ["%_x%"] == [""] echo Use --help for help & echo. & goto :p_show

set "arg1=%1"
set "arg1=%arg1:~0,2%"

if ["%arg1%"] == ["--"] (
	if /i ["%1"] == ["--del"] goto :p_del %*
	if /i ["%1"] == ["--add"] goto :p_add %*
	if /i ["%1"] == ["--list"] goto :p_show %2
	if /i ["%1"] == ["--help"] goto :p_help
) else (
	goto :p_jump %1
)

goto :eof

:p_show
type "%JUMPS%" || echo No jumps defined
goto :eof

:p_add
findstr /b /v /i "%2=" "%JUMPS%" >> "%JUMPS%.tmp"
type "%JUMPS%.tmp" > "%JUMPS%" & @del /f /q "%JUMPS%.tmp"
echo %2=%CD%>>%JUMPS%
goto :eof

:p_del
findstr /b /v /i "%2=" "%JUMPS%" >> "%JUMPS%.tmp"
type "%JUMPS%.tmp" > "%JUMPS%" & @del /f /q "%JUMPS%.tmp"
goto :eof

:p_jump
set JUMP_FOUND=0

for /f "tokens=2* delims==" %%p in ('findstr /b /i "%1=" "%JUMPS%"') do (
	set JUMP_FOUND=1
	echo.Jump to %%p
	endlocal
	chdir /d "%%p"
)

if [%JUMP_FOUND%] == [0] (
	echo Jump '%1' not defined.
	echo.
	goto :p_show %1
)
goto :eof

:p_help
echo.Usage %0 [--add ^| --del ^| --list ^| --help] ^<JUMP NAME^>
echo.	eg.	%0 --add home : adds called directory as jump home.
echo.		%0 home : changes directory to that jump name.