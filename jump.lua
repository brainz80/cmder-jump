function jump_match_generator(text, first, last)
	local line_buffer = rl_state.line_buffer

	found_matches = false
	
	if line_buffer:find("^jump ") or line_buffer:find("^jump%.bat ") then
		has_start_alias = not (
			line_buffer:find("^jump%.bat[ ]*$") or
			line_buffer:find("^jump[ ]*$")
		)

		for line in io.popen("jump --list"):lines() do
			local s, e = line:find("=", 1, true)
			local alias = line:sub(0, s-1)

			if not has_start_alias then
				clink.add_match(alias)
				found_matches = true
			elseif #text > 0 and alias:find(text) then
				clink.add_match(alias)
				found_matches = true
			end
		end
	end

	return found_matches
end

jump_parser = clink.arg.new_parser()
jump_parser:set_flags("--add", "--del", "--list", "--help")

clink.arg.register_parser("jump.bat", jump_parser)
clink.arg.register_parser("jump", jump_parser)

clink.register_match_generator(jump_match_generator, 10)